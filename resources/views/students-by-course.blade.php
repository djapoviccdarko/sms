<div class="card mb-3">
    <div class="card-body">
        <h5 class="card-title">Lista studenata</h5>
        <p class="card-text">Informacije o studentima</p>
        <table class="table">
            <thead class="thead-light">
            <tr>
                <th scope="col">Broj indeksa</th>
                <th scope="col">Ime</th>
                <th scope="col">Prezime</th>
                <th scope="col">Godina rođenja</th>
                <th scope="col">Smer</th>
                <th scope="col">Izmena</th>
            </tr>
            </thead>
            @foreach ($students as $student)
                <tbody>
                <tr>
                    <td>{{ $student->number }}</td>
                    <td>{{ $student->firstName }}</td>
                    <td>{{ $student->lastName }}</td>
                    <td>{{ $student->birthDate }}</td>
                    <td>{{ $student->course_name }}</td>
                    <td>
                        <a href="{{url('/show/'.$student->id)}}" class="btn btn-sm btn-info">Detaljan prikaz</a>
                        @if(Auth::check())
                            <a href="{{url('/destroy/'.$student->id)}}" class="btn btn-sm btn-danger">Izbriši</a>
                        @endif
                    </td>
                </tr>
                @endforeach
                </tbody>
        </table>
    </div>
</div>
