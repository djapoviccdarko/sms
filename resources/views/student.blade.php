<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css"
          integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <title>Sistem evidencije studenata</title>
</head>

<body>
@include('navbar')
<div class="row header-container justify-content-center">
    <div class="header">
        <h1>Evidencija studenata</h1>
    </div>
</div>
@if ($layout == 'index')
    <div class="container-fluid mt-4">
        <div class="row">

            <section class="col-md-5">
                <div class="card mb-3">
                    <div class="card-body">
                        <h5 class="card-title">Pregled studenata po smeru</h5>

                        <div class="d-grid gap-2">

                            @foreach ($courses as $course)

                                <a href="{{ route('students.by.course', ['course' => $course->name]) }}"
                                   class="btn btn-info btn-lg btn-block">{{ $course->name }}</a>

                            @endforeach
                        </div>
                    </div>
                </div>
            </section>

            <section class="col-md-7">
                @include('student-list')
            </section>
        </div>
    </div>
@elseif($layout == 'create')
    <div class="container-fluid mt-4">
        <div class="row justify-content-center">
            <section class="col-md-5">
                <div class="card mb-3">
                    <div class="card-body">
                        <h5 class="card-title">Uneti informacije o novom studentu</h5>
                        <form action="{{ url('store') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label>Broj Indeksa</label>
                                <input name="number" type="text" class="form-control"
                                       placeholder="Uneti broj indeksa">
                            </div>
                            <div class="form-group">
                                <label>Ime</label>
                                <input name="firstName" type="text" class="form-control"
                                       placeholder="Uneti ime">
                            </div>
                            <div class="form-group">
                                <label>Prezime</label>
                                <input name="lastName" type="text" class="form-control"
                                       placeholder="Uneti prezime">
                            </div>

                            <div class="form-group">
                                <label>Godina rođenja</label>
                                <input name="birthDate" type="number" class="form-control"
                                       placeholder="Uneti godinu rođenja">
                            </div>
                            <div class="form-group">
                                <label>Smer</label>
                                @foreach($courses as $course)
                                    <div class="form-check">
                                        <input
                                            type="radio"
                                            name="course"
                                            id="course_{{ $course->id }}"
                                            class="form-check-input"
                                            value="{{ $course->id }}"
                                        >
                                        <label for="course_{{ $course->id }}" class="form-check-label">
                                            {{ $course->name }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                            <div class="form-group">
                                <label>Mesto rođenja</label>
                                <input name="birth_place" type="text" class="form-control"
                                       placeholder="Uneti mesto rođenja">
                            </div>
                            <div class="form-group">
                                <label>Adresa</label>
                                <input name="address" type="text" class="form-control"
                                       placeholder="Uneti Adresu">
                            </div>
                            <div class="form-group">
                                <label>Broj telefona</label>
                                <input name="phone_number" type="text" class="form-control"
                                       placeholder="Uneti broj telefona">
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <input name="status" type="text" class="form-control"
                                       placeholder="Budžet/Samofinansiranje">
                            </div>
                            <input type="submit" class="btn btn-info" value="Sačuvaj">


                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>

    </section>
@elseif($layout == 'students-by-course')
    <div class="container-fluid mt-4">
        <div class="row justify-content-center">
            <section class="col-md-7">
                @include('students-by-course')
            </section>
        </div>
    </div>

@elseif($layout == 'show')
    <div class="container-fluid mt-4">
        <div class="row justify-content-center">
            <section class="col-md-8">
                @include('student-details')
            </section>
        </div>
    </div>
@elseif($layout == 'destroy')
    <div class="container-fluid mt-4">
        <div class="row">
            <section class="col-md-7">
                @include('student-list')
            </section>
            <section class="col-md-5">

            </section>
        </div>
    </div>
@elseif($layout == 'edit')
    <div class="container-fluid mt-4">
        <div class="row">
            <section class="col-md-7">
                @include('student-edit')
            </section>
            <section class="col-md-5">
                <div class="card mb-3">

                    <div class="card-body">
                        <h5 class="card-title">Ažuriranje informacija o studentu</h5>
                        <form action="{{ url('/update/' . $student->id) }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label>Broj indeksa</label>
                                <input value="{{ $student->number }}" name="number" type="text"
                                       class="form-control" placeholder="Uneti broj indeksa">
                            </div>
                            <div class="form-group">
                                <label>Ime</label>
                                <input value="{{ $student->firstName }}" name="firstName" type="text"
                                       class="form-control" placeholder="Uneti ime">
                            </div>
                            <div class="form-group">
                                <label>Prezime</label>
                                <input value="{{ $student->lastName }}" name="lastName" type="text"
                                       class="form-control" placeholder="Uneti prezime">
                            </div>

                            <div class="form-group">
                                <label>Godina rođenja</label>
                                <input value="{{ $student->birthDate }}" name="birthDate" type="text"
                                       class="form-control" placeholder="Uneti godinu rođenja">
                            </div>
                            <div class="form-group">
                                <label>Smer</label>
                                @foreach($courses as $course)
                                    <div class="form-check">
                                        <input
                                            type="radio"
                                            name="course"
                                            id="course_{{ $course->id }}"
                                            class="form-check-input"
                                            value="{{ $course->id }}"
                                        >
                                        <label for="course_{{ $course->id }}" class="form-check-label">
                                            {{ $course->name }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                            <div class="form-group">
                                <label>Mesto rođenja</label>
                                <input value="{{ $student->birth_place }}" name="birth_place" type="text"
                                       class="form-control" placeholder="Uneti mesto rođenja">
                            </div>
                            <div class="form-group">
                                <label>Adresa</label>
                                <input value="{{ $student->address }}" name="address" type="text"
                                       class="form-control" placeholder="Uneti adresu">
                            </div>
                            <div class="form-group">
                                <label>Broj telefona</label>
                                <input value="{{ $student->phone_number }}" name="phone_number" type="text"
                                       class="form-control" placeholder="Uneti broj telefona">
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <input value="{{ $student->status }}" name="status" type="text"
                                       class="form-control" placeholder="Budžet/Samofinansiranje">
                            </div>
                            <input type="submit" class="btn btn-info" value="Ažuriraj">

                        </form>
                    </div>
                </div>

            </section>
        </div>
    </div>
@endif
<footer class="fixed-bottom"></footer>


</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
</html>
