<div class="card mb-3">
    <div class="card-body">
        <h5 class="card-title"><h1>{{ $student['firstName'] ." ". $student['lastName']}}</h1></h5>
        <p class="card-text">Broj indeksa: {{$student['number']}}</p>
        <table class="table">
            <thead class="thead-light">
            <tr>
                <th scope="col">Ime</th>
                <th scope="col">Prezime</th>
                <th scope="col">Godina rođenja</th>
                <th scope="col">Smer</th>
                <th scope="col">Mesto rođenja</th>
                <th scope="col">Adresa</th>
                <th scope="col">Broj telefona</th>
                <th scope="col">Status</th>
                <th scope="col">Prosečna ocena</th>
                @if(Auth::check())
                    <th scope="col">Izmena</th>
                @endif
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{ $student['firstName'] }}</td>
                <td>{{ $student['lastName'] }}</td>
                <td>{{ $student['birthDate'] }}</td>
                <td>{{ $student['course_name'] }}</td>
                <td>{{ $student['birth_place'] }}</td>
                <td>{{ $student['address'] }}</td>
                <td>{{ $student['phone_number'] }}</td>
                <td>{{ $student['status'] }}</td>
                <td>{{ $averageRating }}</td>
                @if(Auth::check())
                    <td>
                        <a href="{{url('/edit/'.$student->id)}}" class="btn btn-sm btn-warning">Izmeni</a>
                        <a href="{{url('/destroy/'.$student->id)}}" class="btn btn-sm btn-danger">Obriši</a>
                    </td>
                @endif
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="card mb-3">
    <div class="card-body">
        <h5 class="card-title"><h1>Lista predmeta</h1></h5>

        <table class="table">
            <thead>
            <tr>
                <th>Naziv predmeta</th>
                <th>Profesor</th>
                <th>Ocena</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($subjects as $subject)
                <tr>
                    <td>{{ $subject->name }}</td>
                    <td>{{ $subject->professor }}</td>
                    <td>
                        @if (!is_null($subject->pivot->rating))
                            {{ $subject->pivot->rating }}
                        @else
                            /
                        @endif
                    </td>

                    @if ($subject->pivot->is_rated)
                        <td style="background-color: mediumseagreen">
                            Položen
                        </td>
                    @else
                        <td style="background-color: orangered">
                            Nije Položen
                        </td>
                    @endif
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
</div>
