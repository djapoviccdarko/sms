<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Student;
use App\Models\Subject;
use Egulias\EmailValidator\EmailValidator;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use PHPUnit\Framework\MockObject\Builder\Stub;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $courses = Course::all();
        $students = Student::join('courses', 'students.course_id', '=', 'courses.id')
            ->select('students.*', 'courses.name')
            ->get();
        return view('student', ['students' => $students, 'layout' => 'index'], ['courses' => $courses]);

    }


    public function studentsByCourse($course)
    {
        $course = Course::where('name', $course)->firstOrFail();

        $students = Student::join('courses', 'students.course_id', '=', 'courses.id')
            ->select('students.*', 'courses.name as course_name')
            ->where('students.course_id', $course->id)
            ->get();

        return view('student', ['students' => $students, 'layout' => 'students-by-course']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        $courses = Course::all();

        $students = Student::all();
        return view('student', ['students' => $students, 'layout' => 'create'], ['courses' => $courses]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $student = new Student();
        $student->number = $request->input('number');
        $student->firstName = $request->input('firstName');
        $student->lastName = $request->input('lastName');
        $student->birthDate = $request->input('birthDate');
        $student->course_id = $request->input('course');
        $student->birth_place = $request->input('birth_place');
        $student->address = $request->input('address');
        $student->phone_number = $request->input('phone_number');
        $student->status = $request->input('status');
        $student->save();
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function show($id)
    {
        $student = Student::join('courses', 'students.course_id', '=', 'courses.id')
            ->select('students.*', 'courses.name as course_name')
            ->where('students.id', $id)
            ->first();

        $subjects = Student::find($id)->subjects()->get();

        $totalRating = 0;
        $numberOfSubjects = 0;

        foreach ($subjects as $subject) {
            if (!is_null($subject->pivot->rating)) {
                $totalRating += $subject->pivot->rating;
                $numberOfSubjects++;
            }
        }

        $averageRating = ($numberOfSubjects > 0) ? $totalRating / $numberOfSubjects : null;
        $averageRating = number_format($averageRating, 2, '.', '');

        return view('student', ['student' => $student, 'subjects' => $subjects, 'averageRating' => $averageRating, 'layout' => 'show']);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $courses = Course::all();

        $student = Student::find($id);
        $students = Student::join('courses', 'students.course_id', '=', 'courses.id')
            ->select('students.*', 'courses.name as course_name')
            ->where('students.id', $id)
            ->get();
        return view('student', ['students' => $students, 'student' => $student, 'layout' => 'edit'], ['courses' => $courses]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $student = Student::find($id);
        $student->number = $request->input('number');
        $student->firstName = $request->input('firstName');
        $student->lastName = $request->input('lastName');
        $student->birthDate = $request->input('birthDate');
        $student->course_id = $request->input('course');
        $student->birth_place = $request->input('birth_place');
        $student->address = $request->input('address');
        $student->phone_number = $request->input('phone_number');
        $student->status = $request->input('status');
        $student->save();
        return redirect('show/' . $student->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $student = Student::find($id);
        $student->delete();
        return redirect('/');
    }
}
